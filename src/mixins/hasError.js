import _ from 'lodash'

export default {
  inject: {
    tab: { default: false }
  },

  data () {
    return {
      invalid: false
    }
  },

  props: {
    error: {
      type: [Array],
      default: () => []
    }
  },

  watch: {
    error () {
      if (_.get(this.error, 'length')) {
        this.addError()
      } else {
        this.resetError()
      }
    }
  },

  methods: {
    resetError () {
      this.invalid = false

      if (this.tab) {
        this.$store.commit('REMOVE_TAB_ERROR', {
          tab: this.tab,
          error: { txt: _.get(this.error, '[0]'), id: this._uid }
        })
      }
    },

    addError () {
      this.invalid = !!this.error[0]

      if (this.tab && _.get(this.error, '[0]')) {
        this.$store.commit('ADD_TAB_ERROR', {
          tab: this.tab,
          error: { txt: this.error[0], id: this._uid }
        })
      }
    }
  },

  beforeDestroy () {
    if (this.invalid) {
      this.resetError()
    }
  }
}
